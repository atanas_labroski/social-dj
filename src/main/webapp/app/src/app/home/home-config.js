(function(){
    angular.module('sdj.modules.home', [])

        .config(function config($stateProvider) {
            $stateProvider.state('home', {
                url: '/home',
                views: {
                    "main": {
                        controller: 'HomeController',
                        controllerAs: 'homeController',
                        templateUrl: 'home/home.tpl.html'
                    }
                },
                data: {pageTitle: 'Home'}
            });
        });
})();