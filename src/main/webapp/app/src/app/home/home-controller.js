(function(){
'use strict';

    function HomeController(homeService, redirectService){
        var viewModel = this;

        viewModel.home = {};

        viewModel.init = function () {
            homeService.getHome().then(function (data) {
                viewModel.home.id = data.id;
                viewModel.home.content = data.content;
            });
        };

        viewModel.newPlaylist = function () {
            homeService.newPlaylist().then(
                function (result) {
                    if (result.data.urlId != null && result.data.urlId !== undefined) {
                        redirectService.goToNewPlaylist(result.data.urlId);
                    } else {
                        console.log("error creating new playlist");
                    }
                }, function (result) {
                    console.log(result);
                });
        };

        viewModel.init();
    }

    angular.module('sdj.modules.home').controller('HomeController', HomeController);
})();