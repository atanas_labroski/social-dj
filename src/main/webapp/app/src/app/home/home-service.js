(function () {
    'use strict';

    angular.module('sdj.modules.home').factory('homeService', function ($http) {

        var factory = {};

        factory.getHome = function () {
            return $http.get("/home");
        };

        factory.newPlaylist = function () {
            return $http.get("/api/newPlaylist");
        };

        return factory;

    });

})();