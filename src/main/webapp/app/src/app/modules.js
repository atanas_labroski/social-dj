(function () {
    'use strict';
    angular.module('sdj.modules', [
        'sdj.modules.common',
        'sdj.modules.home',
        'sdj.modules.user',
        'sdj.modules.register',
        'sdj.modules.playlist'
    ]);
})();