(function () {


    angular.module("sdj.modules.user", [])

        .config(function config($stateProvider) {
            $stateProvider.state('login', {
                url: '/login',
                views: {
                    "main": {
                        controller: "UserController",
                        templateUrl: "user/login.tpl.html"
                    }
                },
                data: {pageTitle: 'Home'}

            });
        })

        .controller('UserController', function UserController($rootScope, $scope, $http, $location) {
            var authenticate = function (callback) {

                $http.get('/user').success(function (data) {
                    if (data.auth == "ok") {
                        $rootScope.authenticated = true;
                    } else {
                        $rootScope.authenticated = false;
                    }
                    if (callback) {
                        callback();
                    }
                }).error(function () {
                    $rootScope.authenticated = false;
                    if (callback) {
                        callback();
                    }
                });

            };

            authenticate();

            $scope.login = function () {
                var payload = 'j_username=' + $scope.credentials.username + '&j_password=' + $scope.credentials.password;
                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Cache-Control': "no-cache"
                    }
                };
                $http.post('/j_spring_security_check', payload, config).success(function (data) {
                    console.log(JSON.stringify(data));

                    authenticate(function () {
                        if ($rootScope.authenticated) {
                            $location.path("/home");
                            $scope.error = false;
                        } else {
                            $location.path("/login");
                            $scope.error = true;
                        }
                    });
                }).error(function (data) {
                    console.log(JSON.stringify(data));
                    $location.path("/login");
                    $scope.error = true;
                    $rootScope.authenticated = false;
                });
            };
        });
})();