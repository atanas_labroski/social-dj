(function () {
    
    angular.module('sdj.modules.register', [])
        .factory('registerService', function ($http, $location, $q) {
            var service = {};
            service.register = function (data) {
                var payload = {
                    username: data.username,
                    password: data.password
                };
                var config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'Cache-control': 'no-cache'
                    }
                };
                //var deferred=$q.defer();

                $http.post('/register', payload, config)
                    .success(function (data) {
                        $location.path("/login");
                        // deferred.resolve(data);

                    }).error(function (data) {
                        // deferred.reject(data);
                        alert("Error registering user");

                    });
                // return deferred.promise();
            };

            return service;
        })
        .config(function ($stateProvider) {
            $stateProvider.state('register', {
                url: '/register',
                views: {
                    'main': {
                        controller: 'RegisterController',
                        templateUrl: 'register/register.tpl.html'
                    }
                },
                data: {pageTitle: 'Register'}
            });


        })
        .controller('RegisterController', function ($scope, registerService) {
            $scope.register = function () {
                registerService.register($scope.credentials);

            };

        });
})();