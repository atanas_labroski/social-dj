(function () {
    'use strict';

    angular.module('sdj.modules.common').factory('redirectService', function ($state, $stateParams) {

        return {
            goToNewPlaylist: function (id) {
                console.log(id);
                $state.go("pl", {urlId: id});
            }
        };

    });

})();