(function () {
    'use strict';

    function PlaylistController($scope, playlistService) {
        var API_KEY = "AIzaSyBfrhX5VDYZOV2j8XjkDvalqk6s-GDC2fk";
        var viewModel = this;

        /* Getting data about a video by ID */
        //var YOUTUBE_API_URL = "https://www.googleapis.com/youtube/v3/videos?" +
        //    "id=" + "GaOY4K3App4" +
        //    "&key=" + API_KEY +
        //    "&part=snippet";

        var getYoutubeSearchRelatedVideosURL = function (videoId) {
            return "https://www.googleapis.com/youtube/v3/search?" +
                "relatedToVideoId=" + videoId +
                "&part=snippet" +
                "&type=video" +
                "&key=" + API_KEY;
        };

        var playerId = {
            first: 1,
            second: 2
        };

        viewModel.currentVideo = "-ncIVUXZla8";
        viewModel.nextVideo = "GaOY4K3App4";

        var cueVideo = function (videoId, player) {
            player.cueVideoById(
                {
                    videoId: videoId,
                    startSeconds: 0,
                    suggestedQuality: "large"
                }
            );

            //TODO alabrosk 23 May 2015: implement logic for getting the next video.4
            viewModel.refreshSuggestedList(currentVideo);
        };

        viewModel.currentPlayer = playerId.first;

        viewModel.firstPlayer = {
            playerId: playerId.first,
            player: null,
            videoId: '-ncIVUXZla8'
        };

        viewModel.secondPlayer = {
            playerId: playerId.second,
            player: null,
            videoId: '-ncIVUXZla8'
        };

        $scope.$on('youtube.player.ready', function ($event, player) {
            if (viewModel.currentPlayer == playerId.first) {

                if (player == viewModel.firstPlayer.player) {
                    player.playVideo();
                    viewModel.refreshSuggestedList(viewModel.firstPlayer.videoId);
                } else if (player == viewModel.secondPlayer.player) {
                    cueVideo(viewModel.nextVideo, player);
                }
            } else if (viewModel.currentPlayer == playerId.second) {

                if (player == viewModel.secondPlayer.player) {
                    player.playVideo();
                    viewModel.refreshSuggestedList(viewModel.secondPlayer.videoId);
                } else if (player == viewModel.firstPlayer.player) {
                    cueVideo(viewModel.nextVideo, player);
                }
            }
        });


        $scope.$on('youtube.player.queued', function ($event, player) {
            //TODO alabrosk 23 May 2015: while one player plays music the other should buffer the song.
            player.playVideo();

            setTimeout(function () {
                player.stopVideo();
            }, 10);
        });

        $scope.$on('youtube.player.ended', function ($event, player) {
            viewModel.currentPlayer = viewModel.currentPlayer == playerId.first ? playerId.second : playerId.first;

            if (viewModel.currentPlayer == playerId.first) {
                viewModel.firstPlayer.player.playVideo();

                cueVideo(viewModel.nextVideo, viewModel.secondPlayer.player);

            } else if (viewModel.currentPlayer == playerId.second) {
                viewModel.secondPlayer.player.playVideo();

                cueVideo(viewModel.nextVideo, viewModel.firstPlayer.player);
            }

        });

        viewModel.refreshSuggestedList = function (videoId) {
            playlistService.refreshSuggestedList(getYoutubeSearchRelatedVideosURL(videoId)).then(
                function (data) {
                    console.log(JSON.stringify(data));
                },
                function (data) {
                    console.log(JSON.stringify(data));
                });
        };

    }

    angular.module('sdj.modules.playlist').controller('PlaylistController', PlaylistController);

})();