(function () {
    'use strict';

    angular.module('sdj.modules.playlist').factory('playlistService', function ($http) {
        return {
            refreshSuggestedList: function (url) {
                return $http.get(url);
            }
        };
    });

})();