angular.module('sdj.modules.playlist', [])

    .config(function ($stateProvider) {
        $stateProvider.state('pl', {
            url: '/pl/:urlId',
            views: {
                'main': {
                    controller: 'PlaylistController',
                    controllerAs: 'playlistController',
                    templateUrl: 'playlist/playlist.tpl.html'
                }
            },
            data: {
                pageTitle: 'Playing Music',
                showHeader: false
            }
        });

    });
