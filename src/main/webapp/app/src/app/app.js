angular.module('sdj', [
    'templates-app',
    'templates-common',
    'youtube-embed',
    'ui.router',
    'sdj.modules'
])

    .config(function myAppConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');
    })

    .run(function run($rootScope) {
        $rootScope.showHeader = true;
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $rootScope.pageTitle = toState.data.pageTitle;
            }
            if (angular.isDefined(toState.data.showHeader)) {
                $rootScope.showHeader = toState.data.showHeader;
            } else {
                $rootScope.showHeader = true;
            }
        });
    });
