package com.alab.service;

import com.alab.model.User;

/**
 * Created by al on 1/17/15.
 */
public interface UserService {

    public User findUserById(Long id);
    public User findUserByUsername(String username);
    public void createUser(User user);
    public boolean login(User user);
}
