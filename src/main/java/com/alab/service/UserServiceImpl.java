package com.alab.service;

import com.alab.model.User;
import com.alab.repository.UserRepository;
import com.alab.service.exceptions.UserExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Created by al on 1/17/15.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findUserById(Long id) {
        return userRepository.findUserById(id);
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public void createUser(User user) {
        User u = userRepository.findUserByUsername(user.getUsername());
        if (u == null) {
            userRepository.createUser(user);
        } else {
            throw new UserExistsException();
        }

    }

    @Override
    public boolean login(User user) {
        return userRepository.login(user);
    }


}
