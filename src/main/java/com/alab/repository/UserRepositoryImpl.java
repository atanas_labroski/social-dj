package com.alab.repository;

import com.alab.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by al on 1/17/15.
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User findUserById(Long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public void createUser(User user) {
        entityManager.persist(user);
    }

    @Override
    public boolean login(User user) {
        return false;
    }

    @Override
    public User findUserByUsername(String username) {

        /* Check if exits user in db with this username */
        Query query=entityManager.createQuery("SELECT u FROM User u WHERE u.username=?1");
        query.setParameter(1,username);
        List<User> users=query.getResultList();
        if(users.size()==0){
            return  null;
        }else{

            return users.get(0);
        }
    }
}
