package com.alab.repository;

import com.alab.model.Song;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by alabrosk on 5/19/15.
 */
@RepositoryRestResource(collectionResourceRel = "song", path = "song")
public interface SongRepository extends PagingAndSortingRepository<Song, Long> {


}
