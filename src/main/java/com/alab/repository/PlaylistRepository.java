package com.alab.repository;

import com.alab.model.Playlist;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Created by alabrosk on 5/19/15.
 */
@RepositoryRestResource(collectionResourceRel = "playlist", path = "playlist")
public interface PlaylistRepository extends PagingAndSortingRepository<Playlist, Long> {

    public Playlist findByUrlId(String urlId);

    @RestResource(exported = false)
    @Override
    void delete(Long aLong);

    @RestResource(exported = false)
    @Override
    void delete(Iterable<? extends Playlist> iterable);

    @RestResource(exported = false)
    @Override
    void delete(Playlist playlist);

    @RestResource(exported = false)
    @Override
    void deleteAll();
}
