package com.alab.repository;

import com.alab.model.User;

/**
 * Created by al on 1/17/15.
 */
public interface UserRepository {

    public User findUserById(Long id);
    public void createUser(User user);
    public boolean login(User user);
    public User findUserByUsername(String username);

}
