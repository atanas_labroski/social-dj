package com.alab.repository;

import com.alab.model.Playlist;
import com.alab.model.PlaylistSongMN;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by alabrosk on 5/20/15.
 */
@RepositoryRestResource
public interface PlaylistSongMNRepository extends PagingAndSortingRepository<PlaylistSongMN, Long> {

    @Query("select psmn from PlaylistSongMN psmn where psmn.playlist.urlId = ?1")
    List<PlaylistSongMN> findByPlaylistUUID(String id);

}
