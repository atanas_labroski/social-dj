package com.alab.security;

import com.alab.model.User;
import com.alab.repository.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.logging.Logger;

/**
 * Created by petar on 1/27/15.
 */
public class UserDetailsServiceImpl implements UserDetailsService{

    @Autowired
    private UserRepositoryImpl userRepository;

    @Override
    public UserDetailsUser loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("No user found with username: " + username);
        } else {
            //todo add logging here instead of println.
//            System.out.println("user exists with username=" + username);
            return new UserDetailsUser(user);

        }
    }
}
