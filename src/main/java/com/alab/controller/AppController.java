package com.alab.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by al on 3/11/15.
 */
@Controller
public class AppController {
    private static final String INDEX_URL = "index.html";
 
    @RequestMapping(value = "/")
    public String index() {

        return "redirect:" + INDEX_URL;
    }
}
