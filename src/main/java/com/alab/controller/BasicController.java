package com.alab.controller;

import com.alab.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by al on 1/17/15.
 */
@Controller
public class BasicController {

    @RequestMapping("/test")
    public String index() {
        return "hello";
    }

}



