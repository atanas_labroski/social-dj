package com.alab.controller;

import com.alab.model.Playlist;
import com.alab.model.User;
import com.alab.repository.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by al on 1/19/15.
 */
@RestController
public class SimpleSinglePageController {

    @Autowired
    private PlaylistRepository playlistRepository;
    
    @RequestMapping(value = "/user")
    public String authenticationCheck() {

        System.out.println("@RestController /user");
        return "{\"auth\":\"ok\"}";
    }
    
    @RequestMapping(value = "/home")
    public Map<String, Object> home() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", UUID.randomUUID().toString());
        map.put("content", "Simple Single Page Application");
        return map;
    }

    @RequestMapping(value = "/api/newPlaylist")
    public ResponseEntity<Resource<Playlist>> createNewPlaylist() {

        Playlist savedPlaylist = playlistRepository.save(new Playlist());

        Resource<Playlist> playlistResource = new Resource<Playlist>(savedPlaylist);

        ControllerLinkBuilder playlistRepositoryLink = ControllerLinkBuilder.linkTo(PlaylistRepository.class);
        playlistResource.add(new Link(playlistRepositoryLink + "/api/playlist/" + savedPlaylist.getId()));

        return new ResponseEntity<>(playlistResource, HttpStatus.OK);
    }

}
