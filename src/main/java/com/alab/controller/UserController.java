package com.alab.controller;

import com.alab.controller.exceptions.ConflictException;
import com.alab.model.User;
import com.alab.resoruces.UserResource;
import com.alab.service.UserService;
import com.alab.service.UserServiceImpl;
import com.alab.service.exceptions.UserExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by petar on 1/28/2015.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<UserResource> registerUser(@RequestBody UserResource sentUserResource) {
        //@RequestParam(value = "role", defaultValue = "ROLE_USER", required = false) String role
        System.out.println("Register url in UserController");
        try {
            userService.createUser(sentUserResource.toUser());

            return new ResponseEntity<UserResource>(sentUserResource, HttpStatus.OK);
        } catch (UserExistsException userExists) {

            throw new ConflictException(userExists);

        }

    }
}
