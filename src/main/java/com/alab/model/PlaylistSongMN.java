package com.alab.model;

import javax.persistence.*;

/**
 * Created by alabrosk on 5/20/15.
 */
@Entity
public class PlaylistSongMN {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long value;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(updatable = false, nullable = false, name = "song_id")
    private Song song;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private Playlist playlist;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }
}
