package com.alab.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by alabrosk on 5/19/15.
 */
@Entity
public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany
    private List<PlaylistSongMN> playlistSongMNs;

    public Playlist() {
        UUID uuid = UUID.randomUUID();
        String stringUuid = String.valueOf(uuid);
        this.urlId = stringUuid.substring(0, Math.min(stringUuid.length(), 5));
    }

    @Column(unique = true)
    private String urlId;

    public Long getId() {
        return id;
    }

    public String getUrlId() {
        return urlId;
    }

    public List<PlaylistSongMN> getPlaylistSongMNs() {
        return playlistSongMNs;
    }

    public void setPlaylistSongMNs(List<PlaylistSongMN> playlistSongMNs) {
        this.playlistSongMNs = playlistSongMNs;
    }
}
