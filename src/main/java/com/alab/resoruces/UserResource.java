package com.alab.resoruces;

import com.alab.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.transaction.support.ResourceHolderSupport;
import sun.net.ResourceManager;

/**
 * Created by petar on 1/29/2015.
 */
public class UserResource extends ResourceSupport {
    private Long Uid;
    private String username;
    private String password;
    private String role="ROLE_USER";

    public Long getUId() {
        return Uid;
    }

    public void setUId(Long Uid) {
        this.Uid = Uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    @JsonIgnore
    public String getPassword() {
        return password;
    }
    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }
    @JsonIgnore
    public String getRole() {
        return role;
    }
    @JsonProperty
    public void setRole(String role) {
        this.role = role;
    }

    public User toUser(){
        User user=new User();
        user.setId(this.getUId());
        user.setUsername(this.getUsername());
        user.setPassword(this.getPassword());
        user.setRole(this.getRole());
        return user;
    }
}
