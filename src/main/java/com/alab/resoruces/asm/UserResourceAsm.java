package com.alab.resoruces.asm;

import com.alab.model.User;
import com.alab.resoruces.UserResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by petar on 1/29/2015.
 */
public class UserResourceAsm extends ResourceAssemblerSupport<User,UserResource> {
    public UserResourceAsm() {
        super(User.class, UserResource.class);
    }

    @Override
    public UserResource toResource(User user) {
        UserResource userResource=new UserResource();
        userResource.setUId(user.getId());
        userResource.setUsername(user.getUsername());
        userResource.setPassword(user.getPassword());
        userResource.setRole(user.getRole());

        return userResource;
    }
}
