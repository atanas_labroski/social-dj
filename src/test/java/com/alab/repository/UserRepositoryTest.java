package com.alab.repository;

import com.alab.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Created by al on 1/17/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/database-h2-config.xml")
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    private User user;

    @Before
    @Transactional
    @Rollback(false)
    public void setup() {
        user = new User();
        user.setUsername("Username");
        user.setPassword("password");
        userRepository.createUser(user);
    }

    @Test
    @Transactional
    public void testUserRepository() {

        User userById = userRepository.findUserById(user.getId());
        assertNotNull(userById);
        assertEquals(userById.getUsername(), "Username");
        assertEquals(userById.getPassword(), "password");
    }

}
