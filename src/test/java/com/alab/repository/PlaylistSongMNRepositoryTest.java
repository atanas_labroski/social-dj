package com.alab.repository;

import com.alab.Application;
import com.alab.model.Playlist;
import com.alab.model.PlaylistSongMN;
import com.alab.model.Song;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;

/**
 * Created by alabrosk on 5/20/15.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class PlaylistSongMNRepositoryTest {

    public static final String PLAYLIST_ID = "ID123";
    @Autowired
    private PlaylistSongMNRepository playlistSongMNRepository;

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private SongRepository songRepository;

    private PlaylistSongMN playlistSongMN;
    private Song song;
    private Playlist playlist;

    @Before
    public void setup() {
        song = new Song();
        song.setYoutubeId("youtubeId");

        playlist = new Playlist();
//        playlist.setUrlId("ID123");

        playlistSongMN = new PlaylistSongMN();
        playlistSongMN.setPlaylist(playlist);
        playlistSongMN.setSong(song);
        playlistSongMN.setValue(10L);

        playlistRepository.save(playlist);
        songRepository.save(song);
//        playlistSongMNRepository.save(playlistSongMN);
    }

    @Test
    public void shouldInsertIntoDatabase() {
        PlaylistSongMN savedPlaylistSongMN = playlistSongMNRepository.save(playlistSongMN);

        assertNotNull(savedPlaylistSongMN.getId());
        assertNotNull(savedPlaylistSongMN.getPlaylist().getId());
        assertNotNull(savedPlaylistSongMN.getSong().getId());
        assertNotNull(savedPlaylistSongMN.getValue());
        assertThat(savedPlaylistSongMN.getValue(), equalTo(10L));
    }

    @Test
    public void shouldGetAllByPlaylistId() {
        PlaylistSongMN savedPlaylistSongMN = playlistSongMNRepository.save(playlistSongMN);

        List<PlaylistSongMN> playlistSongMNs = playlistSongMNRepository.findByPlaylistUUID(PLAYLIST_ID);

        assertThat(playlistSongMNs.size(), equalTo(1));
    }


}
