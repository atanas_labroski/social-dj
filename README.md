# README #

Before downloading the repository make sure you have:

* git
* maven
* npm (with nodejs)
* grunt
* bower

After downloading the repository first:

* 1. Import maven dependencies.
* 2. Run maven: clean install
* 3. navigate to src/main/webapp/app from command line and run:
*   - npm install 
*   - bower install 
*   - grunt 

Start the web application... Happy coding!! <3
Don't forget to write tests!!


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin